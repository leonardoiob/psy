// Elementi del DOM sui quali lo script agisce

var question1 = document.querySelector('#domanda_1')
var question2 = document.querySelector('#domanda_2')
var question3 = document.querySelector('#domanda_3')
var question4 = document.querySelector('#domanda_4')
var responses = document.querySelectorAll('input')
var goodBye = document.querySelector('#bye')
var submit = document.querySelector('#confirm')

var cloud = document.querySelector('.chat__blurb')

// Nasconde la visibilità degli elementi prima di inizializzare

question1.style.display = "none";
question2.style.display = "none";
question3.style.display = "none";
question4.style.display = "none";

// Array che contiene le varie sezioni con le domande

var questionsArray = [question1, question2, question3, question4, goodBye]


// Funzione che esegue un'animazione che mostra e nasconde la bubble della chat

function showCloud() {
    cloud.style.display = "flex"
    setTimeout(function () {
        cloud.style.display = "none"
    }, 1000)
}


// Funzione che inizializzaza la prima domanda

function initChat() {
    setTimeout(function () {
        question1.style.display = "inline-block";
        cloud.style.display = "none"
    }, 3000)
}

// Inizializza la chat con la funzione sopra

initChat()


// Contatore globale per iterare l'array questionsArray

j = 0;

// Funzione Che mostra la domanda corrente e nasconde la precedente, itera il questionsArray

function display() {

    j += 1
    setTimeout(showCloud(), 1000)
    if (j > 5) {
        question1.style.display = "none"
    }

    setTimeout(function () {
        questionsArray[j].style.display = "block"
        questionsArray[j - 1].style.display = "none"
        console.log(j)
    }, 2000)
}


// Itera la funzione display() per tutti gli elementi dell'array questionsArray

for (i = 0; i < responses.length; i++) {

    responses[i].addEventListener('click', function () {
        display()
    })
}